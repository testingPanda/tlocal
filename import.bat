@echo off
rem ----------------------------------------------------------------------------
rem  Environment variable JAVA_HOME must be set and exported
rem ----------------------------------------------------------------------------

setlocal

if exist "%DENODO_HOME%" SET DENODO_HOME_TEMP4IMPORTDOTBAT=%DENODO_HOME%
if exist "%DENODO_JRE_HOME%" SET DENODO_JRE_HOME_TEMP4IMPORTDOTBAT=%DENODO_JRE_HOME%
if exist "%JAVA_BIN%" SET JAVA_BIN_TEMP4IMPORTDOTBAT=%JAVA_BIN%
if exist "%CLASSPATH%" SET CLASSPATH_TEMP4IMPORTDOTBAT=%CLASSPATH%
SET DENODO_HOME=C:/Program Files (x86)/Denodo Platform

if exist "%DENODO_HOME%" goto configureddenodohome

SET DENODO_HOME=..

:configureddenodohome

SET DENODO_JRE_HOME=%DENODO_HOME%/jre

SET JAVA_BIN=%DENODO_JAVA_HOME%\jre\bin\java.exe
if exist "%JAVA_BIN%" goto configuredjavabin

SET JAVA_BIN=%DENODO_JAVA_HOME%\bin\java.exe
if exist "%JAVA_BIN%" goto configuredjavabin

SET JAVA_BIN=%DENODO_JRE_HOME%\bin\java.exe
if exist "%JAVA_BIN%" goto configuredjavabin

SET JAVA_BIN=%JAVA_HOME%\jre\bin\java.exe
if exist "%JAVA_BIN%" goto configuredjavabin

SET JAVA_BIN=%JAVA_HOME%\bin\java.exe

:configuredjavabin
set CLASSPATH=%DENODO_HOME%\lib\vdp-client-core\denodo-vdp-client.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\vdp-client-core\denodo-vdp-tools.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\log4j.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\commons-cli.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\commons-collections4.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\denodo-commons-util.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\commons-lang.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\commons-codec.jar
set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\lib\contrib\icu4j.jar
if exist "..\conf" if not exist "..\conf\db-tools" set CLASSPATH=%CLASSPATH%;..\conf
if exist "..\conf\db-tools" set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\conf\db-tools
if not exist "..\conf" set CLASSPATH=%CLASSPATH%;%DENODO_HOME%\conf\db-tools

if NOT exist "%JAVA_BIN%" goto label
"%JAVA_BIN%" %JAVA_OPTS% -classpath "%CLASSPATH%" com.denodo.vdb.vdbinterface.client.tools.Import %*
SET EXIT_CODE=%errorlevel%
goto :end

:label
echo "Unable to execute %0: Environment variable JAVA_HOME must be set"
SET EXIT_CODE=1

:end
if exist "%DENODO_HOME_TEMP4IMPORTDOTBAT%" (
SET "DENODO_HOME=%DENODO_HOME_TEMP4IMPORTDOTBAT%"
) else (
SET "DENODO_HOME="
)
if exist "%DENODO_JRE_HOME_TEMP4IMPORTDOTBAT%" (
SET "DENODO_JRE_HOME=%DENODO_JRE_HOME_TEMP4IMPORTDOTBAT%"
) else (
SET "DENODO_JRE_HOME="
)
if exist "%JAVA_BIN_TEMP4IMPORTDOTBAT%" (
SET "JAVA_BIN=%JAVA_BIN_TEMP4IMPORTDOTBAT%"
) else (
SET "JAVA_BIN="
)
if exist "%CLASSPATH_TEMP4IMPORTDOTBAT%" (
SET "CLASSPATH=%CLASSPATH_TEMP4IMPORTDOTBAT%"
) else (
SET "CLASSPATH="
)
SET "DENODO_HOME_TEMP4IMPORTDOTBAT="
SET "DENODO_JRE_HOME_TEMP4IMPORTDOTBAT="
SET "JAVA_BIN_TEMP4IMPORTDOTBAT="
SET "CLASSPATH_TEMP4IMPORTDOTBAT="

exit /B %EXIT_CODE%
